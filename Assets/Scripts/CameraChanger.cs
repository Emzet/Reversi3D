﻿using UnityEngine;

public class CameraChanger : MonoBehaviour
{
    [SerializeField] public Camera Camera2D;
 
    [SerializeField] public Camera Camera3D;
 
    [SerializeField] public KeyCode Key;

    private bool switched;
    void Start()
    {
        Camera2D.gameObject.SetActive(true);
        Camera3D.gameObject.SetActive(false);
        switched = true;
    }
 
    void Update()
    {
        if (Input.GetKeyDown(Key))
        {
            if (switched)
            {
                Camera2D.gameObject.SetActive(false);
                Camera3D.gameObject.SetActive(true);
                switched = false;
            }
            else {
                switched = true;
                Camera2D.gameObject.SetActive(true);
                Camera3D.gameObject.SetActive(false);
            }
        }
    }
}
