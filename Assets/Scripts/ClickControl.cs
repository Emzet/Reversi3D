﻿using UnityEngine;

public class ClickControl : MonoBehaviour
{
    
    private AudioSource source;
    
    public void Start()
    {
        source = GetComponentInChildren<AudioSource>();
        gameObject.transform.Find("Border").gameObject.GetComponent<Renderer>().enabled = false;
    }

    public void OnMouseDown()
    {
        GetComponentInParent<BoardManager>().UpdateSelection();
        if (GetComponentInParent<BoardManager>().IsHumanMove()&&(GetComponentInParent<BoardManager>().IsGoodMove(GetComponentInParent<BoardManager>().selectionX, GetComponentInParent<BoardManager>().selectionY,GetComponentInParent<BoardManager>().currentTurn)))
        {     
            GetComponent<Renderer>().material.color = new Color(1, 1, 1);
            MakeMove(GetComponentInParent<BoardManager>().selectionX, GetComponentInParent<BoardManager>().selectionY);
        }
    }

    //change sqaure color
    public void MakeMove(int x, int y)
    {

            if (GetComponentInParent<BoardManager>().currentTurn == "b")
            {
                //Add black token
                GetComponentInParent<BoardManager>().SpawnTokens(GetComponentInParent<BoardManager>().blackToken, x, y);
            }
            //white turn
            else
            {
                //Add white token
                GetComponentInParent<BoardManager>().SpawnTokens(GetComponentInParent<BoardManager>().whiteToken, x, y);
            }
            source.Play();
            
            //UpdateTokensOnBoard
            GetComponentInParent<BoardManager>().UpdateTokens(x, y);
            GetComponentInParent<BoardManager>().UpdateGoodMovesAll();
            GetComponentInParent<BoardManager>().UpdateTurn();
            GetComponentInParent<BoardManager>().UpdateVisibleMoveOptions();
            GetComponentInParent<BoardManager>().UpdatePoints();
            GetComponentInParent<BoardManager>().UpdateTurnInfo();
            UpdateLastTileHighlight();
            
            GetComponent<BoxCollider>().enabled = false;
    }

    private void UpdateLastTileHighlight()
    {
        if (GetComponentInParent<BoardManager>().lastTile != null)
        {
            GetComponentInParent<BoardManager>().lastTile.gameObject.transform.Find("Border").gameObject.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
            GetComponentInParent<BoardManager>().lastTile.gameObject.transform.Find("Border").gameObject.GetComponent<Renderer>().enabled = false;
        }

        GetComponentInParent<BoardManager>().lastTile = GetComponent<Renderer>().gameObject;

        gameObject.transform.Find("Border").gameObject.GetComponent<Renderer>().material.color = new Color(0, 0, 1);
        gameObject.transform.Find("Border").gameObject.GetComponent<Renderer>().enabled = true;
    }
}