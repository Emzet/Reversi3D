﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class WinnerScreenManager : MonoBehaviour
{
    [SerializeField] private Text scoreText;
    [SerializeField] private Text winnerText;
    [SerializeField] private Text newHighscoreTex;
    private string winner;
    private string nick;
    private string score;
    private int newHighScore;

    private string highScores;

    // Start is called before the first frame update
    void Start()
    {
        winner = PlayerPrefs.GetString("winner");
        score = PlayerPrefs.GetString("score");
        nick = PlayerPrefs.GetString("nick", "");
        newHighScore = PlayerPrefs.GetInt("highscore", 0);
        highScores = PlayerPrefs.GetString("highscores", "");

        scoreText.text = score;
        winnerText.text = winner;
        getHighscores();
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void GoToMainMenu()
    {
        SceneManager.LoadScene(0);
    }

    public void getHighscores()
    {
        HighScore highScore = new HighScore();
        highScore.nick = nick;
        highScore.score = newHighScore;
        HighScore[] highScoreArray;
        if (highScores.Equals(""))
        {
            highScoreArray = new[]{highScore};
        }
        else highScoreArray = JsonHelper.FromJson<HighScore>(highScores);

        List<HighScore> highScoreList = new List<HighScore>(highScoreArray);
        highScoreList.Add(highScore);
        if (highScoreList.Count > 2) highScoreList.Sort((highScore1, highScore2) => highScore2.score.CompareTo(highScore1.score));
        highScoreList.ForEach(highScore1 => highScore1.rank = highScoreList.IndexOf(highScore1));
        if (highScoreList.IndexOf(highScore) < 10)
        {
            newHighscoreTex.text = "NEW HIGHSCORE!";
        }

        if (highScoreList.Count > 10) highScoreList = highScoreList.GetRange(0, 10);

        PlayerPrefs.SetString("highscores", JsonHelper.ToJson(highScoreList.ToArray()));
        PlayerPrefs.DeleteKey("SaveData");
    }

    [Serializable]
    public class HighScore
    {
        public int rank;
        public int score;
        public string nick;
    }


    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
}