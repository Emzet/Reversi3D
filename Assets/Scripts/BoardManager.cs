﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = System.Random;

public class BoardManager : MonoBehaviour
{
    private static readonly float INTERVAL = 0.23F;
    private static readonly float START_HEIGHT = 0.13F;
    private static readonly float START_WIDTH = -0.8F;
    private static readonly float START_FORWARD = -0.8F;
    public GameObject tile;
    public GameObject whiteToken;
    public GameObject blackToken;
    public Camera camera2D;
    public Camera camera3D;
    [SerializeField] public KeyCode cameraChangeKey;
    [SerializeField] public KeyCode escapeKey;
    private Camera _activeCamera;
    private bool _switched;
    private string _resuming;
    public GameObject[,] tokens = new GameObject[8, 8];
    public string[,] tokensStrings = new string[8, 8];
    public string[,] whiteMoves = new string[8, 8];
    public int whiteMovesInt = 99;
    public string[,] blackMoves = new string[8, 8];
    public int blackMovesInt = 99;
    private GameObject[,] _tiles = new GameObject[8, 8];
    private Color _originalTileColor;
    private String _enemy;
    private string _minmax;
    [SerializeField] Text textTurnArea;
    [SerializeField] Text textScoreArea;
    [SerializeField] Text switchingSidesText;
    protected internal string currentTurn = "w";
    private int _whites = 99;
    private int _blacks = 99;
    private static string TRUE = "True";
    private static string FALSE = "False";
    readonly Random _random = new Random();
    public GameObject lastTile;

    //Select X,Y
    protected internal int selectionX = -1;

    protected internal int selectionY = -1;

    // Start is called before the first frame update
    void Start()
    {
        _resuming = PlayerPrefs.GetString("resuming", FALSE);
        camera2D.gameObject.SetActive(true);
        camera3D.gameObject.SetActive(false);
        _activeCamera = camera2D;
        _switched = true;
        StopAllCoroutines();
        CreateInitialTiles();
        if (FALSE.Equals(_resuming))
        {
            StartNewGame();
        }
        else
        {
            if (IsLoadFileValid())
            {
                LoadResumedGame();
            }
            else
            {
                Debug.Log("SAVE FILE CORRUPTED, GOING BACK TO MAIN MENU");
                PlayerPrefs.DeleteKey("SaveData");
                StopAllCoroutines();
                SceneManager.LoadScene(0);
            }
        }

        UpdatePoints();
        UpdateTurnInfo();
        UpdateGoodMovesAll();
        UpdateMovesInts();
        UpdateVisibleMoveOptions();
        StartCoroutine(MakeComputerMove());
    }

    private bool IsLoadFileValid()
    {
        String saveJson = PlayerPrefs.GetString("SaveData");
        SaveData[] saves = JsonHelper.FromJson<SaveData>(saveJson);
        SaveData save = saves[0];
        //load board
        string[,] newBoard = ParseFrom(save.board.ToArray());
        if (newBoard == null || ("").Equals(newBoard)) return false;
        //load nick
        string nick = save.nick;
        if (nick == null) return false;
        //load currentTurn
        currentTurn = save.currentTurn;
        if (currentTurn == null || "".Equals(currentTurn)) return false;
        //load enemy
        _enemy = save.enemy;
        if (_enemy == null || "".Equals(_enemy)) return false;

        //load minmax
        _minmax = save.minmax;
        if (_minmax == null || "".Equals(_minmax)) return false;

        return true;
    }

    private void StartNewGame()
    {
        CreateInitialTokens();
        GetEnemySettings();
    }

    private void LoadResumedGame()
    {
        String saveJson = PlayerPrefs.GetString("SaveData");
        SaveData[] saves = JsonHelper.FromJson<SaveData>(saveJson);
        SaveData save = saves[0];
        //load board
        String[] tmpTokenStrings = save.board.ToArray();
        tokensStrings = ParseFrom(tmpTokenStrings);
        UpdateMap();
        //load nick
        PlayerPrefs.SetString("nick", save.nick);
        //load currentTurn
        currentTurn = save.currentTurn;
        //load enemy
        _enemy = save.enemy;
        //load minmax
        _minmax = save.minmax;
    }

    private string[,] ParseFrom(string[] tmpTokenStrings)
    {
        string[,] tempArray = new String[8, 8];
        int x = 0, y = 0;
        foreach (var tmpTokenString in tmpTokenStrings)
        {
            tempArray[x, y] = tmpTokenString;
            y++;
            if (y == 8)
            {
                y = 0;
                x++;
            }
        }

        return tempArray;
    }

    private void UpdateMap()
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if ("w".Equals(tokensStrings[x, y]))
                {
                    SpawnTokens(whiteToken, x, y);
                }
                else if ("b".Equals(tokensStrings[x, y]))
                {
                    SpawnTokens(blackToken, x, y);
                }
            }
        }
    }

    private void GetEnemySettings()
    {
        _enemy = PlayerPrefs.GetString("enemy");
        _minmax = PlayerPrefs.GetString("minmax", TRUE);
        Debug.Log("Starting game, enemy = " + _enemy + "\n minmax = " + _minmax);
    }

    private void CreateInitialTokens()
    {
        //Spawn first 4 tokens in center
        SpawnTokens(whiteToken, 3, 4);
        SpawnTokens(blackToken, 4, 4);
        SpawnTokens(blackToken, 3, 3);
        SpawnTokens(whiteToken, 4, 3);
    }

    private void CreateInitialTiles()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                tokens[i, j] = null;
                _tiles[i, j] = Instantiate(tile,
                    new Vector3(START_WIDTH + transform.position.x + (i * INTERVAL), transform.position.y + START_HEIGHT, START_FORWARD + transform.position.z + (j * INTERVAL)),
                    tile.transform.rotation,
                    transform);
            }
        }
    }


    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(escapeKey))
        {
            EscapeAndSave();
        }

        if (Input.GetKeyDown(cameraChangeKey))
        {
            if (_switched)
            {
                camera2D.gameObject.SetActive(false);
                camera3D.gameObject.SetActive(true);
                _switched = false;
                _activeCamera = camera3D;
            }
            else
            {
                _switched = true;
                camera2D.gameObject.SetActive(true);
                camera3D.gameObject.SetActive(false);
                _activeCamera = camera2D;
            }
        }

        if (GameIsFinished())
        {
            EndGame();
        }
    }

    private void EndGame()
    {
        string winner;
        string score = _whites + " : " + _blacks;
        if (_blacks > _whites) winner = "Blacks\n win";
        else if (_whites > _blacks) winner = "Whites\n win";
        else winner = "DRAW";
        PlayerPrefs.SetString("winner", winner);
        PlayerPrefs.SetString("score", score);
        PlayerPrefs.SetString("score", score);
        if ("human".Equals(_enemy))
        {
            PlayerPrefs.SetInt("highscore", Math.Max(_blacks, _whites));
        }
        else
            PlayerPrefs.SetInt("highscore", _whites);

        GetEndScreen();
    }

    private void EscapeAndSave()
    {
        //save
        SaveData saveData = new SaveData();
        //save board
        saveData.board = tokensStrings.Cast<String>().ToList();
        //save nick
        saveData.nick = PlayerPrefs.GetString("nick");
        //save currentTurn
        saveData.currentTurn = currentTurn;
        //save enemy
        saveData.enemy = _enemy;
        //save minmax
        saveData.minmax = _minmax;
        //exit
        SaveData[] saveDatas = {saveData};
        String saveDataJson = JsonHelper.ToJson(saveDatas);
        PlayerPrefs.SetString("SaveData", saveDataJson);
        SceneManager.LoadScene(0);
    }

    private IEnumerator MakeComputerMove()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);
            if (currentTurn == "b" && _enemy.Equals("computer"))
            {
                if (blackMovesInt == 0)
                {
                    UpdateTurn();
                }

                if (_minmax.Equals(FALSE)) MakeRandomMove();
                else if (_minmax.Equals(TRUE)) MakeMinMaxMove("b", tokensStrings);
            }
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void MakeMinMaxMove(string color, string[,] array)
    {
        int maxPoints = 0;

        string[,] tmpArray = new string[8, 8];
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                tmpArray[x, y] = array[x, y];
            }
        }


        int maxX = -1;
        int maxY = -1;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (IsSomethingAround(x, y, tmpArray) && "legal".Equals(blackMoves[x, y]))
                {
                    string[,] tmpArray2 = new string[8, 8];
                    for (int j = 0; j < 8; j++)
                    {
                        for (int k = 0; k < 8; k++)
                        {
                            tmpArray2[j, k] = tmpArray[j, k];
                        }
                    }

                    tmpArray2[x, y] = color;
                    UpdateTokensOnArray(x, y, tmpArray2);
                    int tmpPoints = ReadArrayPoints(color, tmpArray2);
                    if (maxPoints < tmpPoints)
                    {
                        maxPoints = tmpPoints;
                        maxX = x;
                        maxY = y;
                    }
                }
            }
        }

        if (maxX != -1 && maxY != -1)
        {
            _tiles[maxX, maxY].GetComponent<ClickControl>().MakeMove(maxX, maxY);
        }
        else MakeRandomMove();
    }

    private int ReadArrayPoints(string color, string[,] array)
    {
        int max = 0;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (color.Equals(array[x, y])) max++;
            }
        }

        return max;
    }

    private void MakeRandomMove()
    {
        int max = 8;

        int randomX;
        int randomY;
        while (true)
        {
            randomX = _random.Next(max);
            randomY = _random.Next(max);
            if ((tokensStrings[randomX, randomY] == null || "".Equals(tokensStrings[randomX, randomY])) && IsSomethingAround(randomX, randomY, tokensStrings) &&
                "legal".Equals(blackMoves[randomX, randomY])) break;
        }

        // ReSharper disable once Unity.PerformanceCriticalCodeInvocation
        _tiles[randomX, randomY].GetComponent<ClickControl>().MakeMove(randomX, randomY);
    }

    private bool GameIsFinished()
    {
        return (blackMovesInt == 0 && whiteMovesInt == 0) || (_blacks + _whites == 8 * 8);
    }

    public void UpdateSelection()
    {
        if (!_activeCamera) return;
        //Get X,Y only from board
        Ray ray = _activeCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out var hit))
        {
            Transform objectHit = hit.transform;
            bool found = false;
            for (int x = 0; x < 8; ++x)
            {
                for (int y = 0; y < 8; ++y)
                {
                    if (_tiles[x, y].transform.Equals(objectHit))
                    {
                        selectionX = x;
                        selectionY = y;
                        found = true;
                        break;
                    }
                }

                if (found) break;
            }
        }
        else
        {
            selectionX = -1;
            selectionY = -1;
        }
    }

    public void SpawnTokens(GameObject token, int x, int y)
    {
        tokens[x, y] = Instantiate(token,
            new Vector3(START_WIDTH + transform.position.x + (x * INTERVAL), transform.position.y + START_HEIGHT, START_FORWARD + transform.position.z + (y * INTERVAL)),
            Quaternion.identity, transform);
        tokens[x, y].name = token.name;
        if (tokens[x, y].gameObject.name.Contains("Biały")) tokensStrings[x, y] = "w";
        else if (tokens[x, y].gameObject.name.Contains("Czarny")) tokensStrings[x, y] = "b";
        _tiles[x, y].GetComponent<BoxCollider>().enabled = false;
        //go.transform.SetParent(transform);  
    }

    public static bool IsSomethingAround(int x, int y, string[,] array)
    {
        if (array[x, y] != null && !"".Equals(array[x, y])) return false;
        if (x == 0)
        {
            if (y == 0)
            {
                return (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) || //up
                       (array[x + 1, y + 1] != null && !"".Equals(array[x + 1, y + 1])) || //upper right
                       array[x + 1, y] != null && !"".Equals(array[x + 1, y]); //right
            }

            if (y == 7)
            {
                return (array[x + 1, y] != null && !"".Equals(array[x + 1, y])) || //right
                       (array[x, y - 1] != null && !"".Equals(array[x, y - 1])) || //bottom
                       array[x + 1, y - 1] != null && !"".Equals(array[x + 1, y - 1]); //bottom right
            }

            return (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) || //up
                   (array[x + 1, y + 1] != null && !"".Equals(array[x + 1, y + 1])) || //upper right
                   (array[x + 1, y] != null && !"".Equals(array[x + 1, y])) || //right
                   (array[x, y - 1] != null && !"".Equals(array[x, y - 1])) || //bottom
                   array[x + 1, y - 1] != null && !"".Equals(array[x + 1, y - 1]); //bottom right
        }

        if (x == 7)
        {
            if (y == 0)
            {
                return (array[x - 1, y + 1] != null && !"".Equals(array[x - 1, y + 1])) || //upper left
                       (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) || //up
                       array[x - 1, y] != null && !"".Equals(array[x - 1, y]); //left
            }

            if (y == 7)
            {
                return (array[x - 1, y] != null && !"".Equals(array[x - 1, y])) || //left
                       (array[x - 1, y - 1] != null && !"".Equals(array[x - 1, y - 1])) || //bottom left
                       array[x, y - 1] != null && !"".Equals(array[x, y - 1]); //bottom
            }

            return (array[x - 1, y + 1] != null && !"".Equals(array[x - 1, y + 1])) || //upper left
                   (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) || //up
                   (array[x - 1, y] != null && !"".Equals(array[x - 1, y])) || //left
                   (array[x - 1, y - 1] != null && !"".Equals(array[x - 1, y - 1])) || //bottom left
                   array[x, y - 1] != null && !"".Equals(array[x, y - 1]); //bottom
        }

        if (y == 0)
        {
            return (array[x - 1, y + 1] != null && !"".Equals(array[x - 1, y + 1])) || //upper left
                   (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) || //up
                   (array[x + 1, y + 1] != null && !"".Equals(array[x + 1, y + 1])) || //upper right
                   (array[x - 1, y] != null && !"".Equals(array[x - 1, y])) || //left
                   array[x + 1, y] != null && !"".Equals(array[x + 1, y]); //right
        }

        if (y == 7)
        {
            return (array[x - 1, y] != null && !"".Equals(array[x - 1, y])) || //left
                   (array[x + 1, y] != null && !"".Equals(array[x + 1, y])) || //right
                   (array[x - 1, y - 1] != null && !"".Equals(array[x - 1, y - 1])) || //bottom left
                   (array[x, y - 1] != null && !"".Equals(array[x, y - 1])) || //bottom
                   array[x + 1, y - 1] != null && !"".Equals(array[x + 1, y - 1]);
        }

        return (array[x - 1, y + 1] != null && !"".Equals(array[x - 1, y + 1])) ||
               (array[x, y + 1] != null && !"".Equals(array[x, y + 1])) ||
               (array[x + 1, y + 1] != null && !"".Equals(array[x + 1, y + 1])) ||
               (array[x - 1, y] != null && !"".Equals(array[x - 1, y])) ||
               (array[x + 1, y] != null && !"".Equals(array[x + 1, y])) ||
               (array[x - 1, y - 1] != null && !"".Equals(array[x - 1, y - 1])) ||
               (array[x, y - 1] != null && !"".Equals(array[x, y - 1])) ||
               (array[x + 1, y - 1] != null && !"".Equals(array[x + 1, y - 1]));
    }

    public void UpdateTokens(int x, int y)
    {
        UpdateUp(x, y);
        UpdateUpRight(x, y);
        UpdateRight(x, y);
        UpdateDownRight(x, y);
        UpdateDown(x, y);
        UpdateDownLeft(x, y);
        UpdateLeft(x, y);
        UpdateUpLeft(x, y);
    }

    private void UpdateUpLeft(int x, int y)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX < 8 && tempY >= 0; tempX++, tempY--)
        {
            if (tokens[tempX, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name)
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX < lastX && tempY > lastY; tempX++, tempY--)
        {
            if (tokens[tempX, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, tempY]);
                SpawnTokens(tokens[x, y].gameObject, tempX, tempY);
            }
        }
    }

    private void UpdateDownLeft(int x, int y)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX < 8 && tempY < 8; tempX++, tempY++)
        {
            if (tokens[tempX, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name)
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX < lastX && tempY < lastY; tempX++, tempY++)
        {
            if (tokens[tempX, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, tempY]);
                SpawnTokens(tokens[x, y].gameObject, tempX, tempY);
            }
        }
    }

    private void UpdateDownRight(int x, int y)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX >= 0 && tempY < 8; tempX--, tempY++)
        {
            if (tokens[tempX, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name)
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX > lastX && tempY < lastY; tempX--, tempY++)
        {
            if (tokens[tempX, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, tempY]);
                SpawnTokens(tokens[x, y].gameObject, tempX, tempY);
            }
        }
    }

    private void UpdateUpRight(int x, int y)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX >= 0 && tempY >= 0; tempX--, tempY--)
        {
            if (tokens[tempX, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name)
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, tempY].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX > lastX && tempY > lastY; tempX--, tempY--)
        {
            if (tokens[tempX, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, tempY]);
                SpawnTokens(tokens[x, y].gameObject, tempX, tempY);
            }
        }
    }

    private void UpdateRight(int x, int y)
    {
        int lastX = -1;

        for (int tempX = x; tempX >= 0; tempX--)
        {
            if (tokens[tempX, y] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, y].gameObject.name)
            {
                lastX = tempX;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, y].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x; tempX > lastX; tempX--)
        {
            if (tokens[tempX, y].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, y]);
                SpawnTokens(tokens[x, y].gameObject, tempX, y);
            }
        }
    }

    private void UpdateLeft(int x, int y)
    {
        int lastX = -1;
        for (int tempX = x; tempX < 8; tempX++)
        {
            if (tokens[tempX, y] == null) break;
            if (tokens[x, y].gameObject.name == tokens[tempX, y].gameObject.name)
            {
                lastX = tempX;
            }

            if (lastX != -1 && (tokens[x, y].gameObject.name == tokens[tempX, y].gameObject.name) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x; tempX < lastX; tempX++)
        {
            if (tokens[tempX, y].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[tempX, y]);
                SpawnTokens(tokens[x, y].gameObject, tempX, y);
            }
        }
    }

    private void UpdateDown(int x, int y)
    {
        int lastY = -1;
        for (int tempY = y; tempY < 8; tempY++)
        {
            if (tokens[x, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[x, tempY].gameObject.name)
            {
                lastY = tempY;
            }

            if (lastY != -1 && (tokens[x, y].gameObject.name == tokens[x, tempY].gameObject.name) && y != tempY) break;
        }

        if (lastY == -1) return;
        for (int tempY = y; tempY < lastY; tempY++)
        {
            if (tokens[x, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[x, tempY]);
                SpawnTokens(tokens[x, y].gameObject, x, tempY);
            }
        }
    }

    private void UpdateUp(int x, int y)
    {
        int lastY = -1;

        for (int tempY = y; tempY >= 0; tempY--)
        {
            if (tokens[x, tempY] == null) break;
            if (tokens[x, y].gameObject.name == tokens[x, tempY].gameObject.name)
            {
                lastY = tempY;
            }

            if (lastY != -1 && (tokens[x, y].gameObject.name == tokens[x, tempY].gameObject.name) && y != tempY) break;
        }

        if (lastY == -1) return;
        for (int tempY = y; tempY > lastY; tempY--)
        {
            if (tokens[x, tempY].gameObject.name != tokens[x, y].name)
            {
                Destroy(tokens[x, tempY]);
                SpawnTokens(tokens[x, y].gameObject, x, tempY);
            }
        }
    }

    public void UpdatePoints()
    {
        _whites = 0;
        _blacks = 0;
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if (tokensStrings[x, y] == null) continue;
                if (tokensStrings[x, y].Equals("w")) _whites++;
                else if (tokensStrings[x, y].Equals("b")) _blacks++;
            }
        }

        textScoreArea.text = "Score: Whites " + _whites + " - " + _blacks + " Blacks";
    }

    public void UpdateTurnInfo()
    {
        string turn = currentTurn.Equals("b") ? "black" : "white";
        textTurnArea.text = "Turn: " + turn;
    }

    public void GetEndScreen()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void UpdateTokensOnArray(int x, int y, string[,] array)
    {
        string color = array[x, y];
        UpdateUpOnArray(x, y, array, color);
        UpdateUpRightOnArray(x, y, array, color);
        UpdateRightOnArray(x, y, array, color);
        UpdateDownRightOnArray(x, y, array, color);
        UpdateDownOnArray(x, y, array, color);
        UpdateDownLeftOnArray(x, y, array, color);
        UpdateLeftOnArray(x, y, array, color);
        UpdateUpLeftOnArray(x, y, array, color);
    }

    private void UpdateUpLeftOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;
        int lastY = -1;

        for (int tempX = x, tempY = y; tempX < 8 && tempY >= 0; tempX++, tempY--)
        {
            if (array[tempX, tempY] == null) break;
            if (array[x, y].Equals(array[tempX, tempY]))
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, tempY])) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX < lastX && tempY > lastY; tempX++, tempY--)
        {
            if (!array[tempX, tempY].Equals(array[x, y]) && !array[tempX, tempY].Equals(""))
            {
                array[tempX, tempY] = color;
            }
        }
    }

    private void UpdateDownLeftOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX < 8 && tempY < 8; tempX++, tempY++)
        {
            if (array[tempX, tempY] == null) break;
            if (array[x, y].Equals(array[tempX, tempY]))
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, tempY])) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX < lastX && tempY < lastY; tempX++, tempY++)
        {
            if (!array[tempX, tempY].Equals(array[x, y]) && !array[tempX, tempY].Equals(""))
            {
                array[tempX, tempY] = color;
            }
        }
    }

    private void UpdateDownRightOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX >= 0 && tempY < 8; tempX--, tempY++)
        {
            if (array[tempX, tempY] == null) break;
            if (array[x, y].Equals(array[tempX, tempY]))
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, tempY])) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX > lastX && tempY < lastY; tempX--, tempY++)
        {
            if (!array[tempX, tempY].Equals(array[x, y]) && !array[tempX, tempY].Equals(""))
            {
                array[tempX, tempY] = color;
            }
        }
    }

    private void UpdateUpRightOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;
        int lastY = -1;


        for (int tempX = x, tempY = y; tempX >= 0 && tempY >= 0; tempX--, tempY--)
        {
            if (array[tempX, tempY] == null) break;
            if (array[x, y].Equals(array[tempX, tempY]))
            {
                lastX = tempX;
                lastY = tempY;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, tempY])) && !tempX.Equals(x)) break;
        }

        if (lastX == -1) return;
        for (int tempX = x, tempY = y; tempX > lastX && tempY > lastY; tempX--, tempY--)
        {
            if (!array[tempX, tempY].Equals(array[x, y]) && !array[tempX, tempY].Equals(""))
            {
                array[tempX, tempY] = color;
            }
        }
    }

    private void UpdateRightOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;

        for (int tempX = x; tempX >= 0; tempX--)
        {
            if (array[tempX, y] == null) break;
            if (array[x, y].Equals(array[tempX, y]))
            {
                lastX = tempX;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, y])) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x; tempX > lastX; tempX--)
        {
            if (!array[tempX, y].Equals(array[x, y]) && !array[tempX, y].Equals(""))
            {
                array[tempX, y] = color;
            }
        }
    }

    private void UpdateLeftOnArray(int x, int y, string[,] array, string color)
    {
        int lastX = -1;
        for (int tempX = x; tempX < 8; tempX++)
        {
            if (array[tempX, y] == null) break;
            if (array[x, y].Equals(array[tempX, y]))
            {
                lastX = tempX;
            }

            if (lastX != -1 && (array[x, y].Equals(array[tempX, y])) && tempX != x) break;
        }

        if (lastX == -1) return;
        for (int tempX = x; tempX < lastX; tempX++)
        {
            if (!array[tempX, y].Equals(array[x, y]) && !array[tempX, y].Equals(""))
            {
                array[tempX, y] = color;
            }
        }
    }

    private void UpdateDownOnArray(int x, int y, string[,] array, string color)
    {
        int lastY = -1;
        for (int tempY = y; tempY < 8; tempY++)
        {
            if (array[x, tempY] == null) break;
            if (array[x, y].Equals(array[x, tempY]))
            {
                lastY = tempY;
            }

            if (lastY != -1 && (array[x, y].Equals(array[x, tempY])) && y != tempY) break;
        }

        if (lastY == -1) return;
        for (int tempY = y; tempY < lastY; tempY++)
        {
            if (!array[x, tempY].Equals(array[x, y]) && !array[x, tempY].Equals(""))
            {
                array[x, tempY] = color;
            }
        }
    }

    private void UpdateUpOnArray(int x, int y, string[,] array, string color)
    {
        int lastY = -1;

        for (int tempY = y; tempY >= 0; tempY--)
        {
            if (array[x, tempY] == null) break;
            if (array[x, y].Equals(array[x, tempY]))
            {
                lastY = tempY;
            }

            if (lastY != -1 && (array[x, y].Equals(array[x, tempY])) && y != tempY) break;
        }

        if (lastY == -1) return;
        for (int tempY = y; tempY > lastY; tempY--)
        {
            if (!array[x, tempY].Equals(array[x, y]) & !array[x, tempY].Equals(""))
            {
                array[x, tempY] = color;
            }
        }
    }

    [Serializable]
    public class SaveData
    {
        public List<string> board;
        public string nick;
        public string enemy;
        public string minmax;
        public string currentTurn;
    }


    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        public static string ToJson<T>(T[] array)
        {
            Wrapper<T> wrapper = new Wrapper<T>();
            wrapper.Items = array;
            return JsonUtility.ToJson(wrapper);
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }

    public bool IsHumanMove()
    {
        return ("w".Equals(currentTurn) || "human".Equals(_enemy));
    }


    public void UpdateGoodMovesAll()
    {
        UpdateGoodMoves("b");
        UpdateGoodMoves("w");
    }

    public void UpdateGoodMoves(string color)
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if ("w".Equals(color))
                    whiteMoves[x, y] = "";
                if ("b".Equals(color))
                    blackMoves[x, y] = "";
            }
        }

        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                string[,] tmpArray2 = new string[8, 8];
                for (int j = 0; j < 8; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        tmpArray2[j, k] = tokensStrings[j, k];
                    }
                }

                if (tmpArray2[x, y] == null || "".Equals(tmpArray2[x, y]) && IsSomethingAround(x, y, tokensStrings))
                {
                    tmpArray2[x, y] = color;

                    int tmpPoints = ReadArrayPoints(color, tmpArray2);
                    UpdateTokensOnArray(x, y, tmpArray2);
                    int finalPoints = ReadArrayPoints(color, tmpArray2);
                    if (finalPoints > tmpPoints)
                    {
                        if ("b".Equals(color))
                        {
                            blackMoves[x, y] = "legal";
                        }
                        else if ("w".Equals(color))
                        {
                            whiteMoves[x, y] = "legal";
                        }
                    }
                }
                else
                {
                    if ("b".Equals(color)) blackMoves[x, y] = "";
                    else if ("w".Equals(color)) whiteMoves[x, y] = "";
                }
            }
        }
    }

    public bool IsGoodMove(int x, int y, string color)
    {
        return "legal".Equals("w".Equals(color) ? whiteMoves[x, y] : blackMoves[x, y]);
    }

    public void UpdateTurn()
    {
        UpdateMovesInts();
        if (!GameIsFinished())
        {
            if ("b".Equals(currentTurn))
            {
                if (whiteMovesInt > 0) currentTurn = "w";
                else PopupSwitchingSidesText("w");
            }
            else
            {
                if (blackMovesInt > 0) currentTurn = "b";
                else PopupSwitchingSidesText("b");
            }
        }
    }

    private void PopupSwitchingSidesText(string color)
    {
        string textToPopup = "No switching sides! \n ";

        if (color.Equals("b")) textToPopup += "No moves available for whites!";
        else textToPopup += "No moves available for blacks!";

        StartCoroutine(ShowMessage(textToPopup, 2));
    }

    IEnumerator ShowMessage(string message, float delay)
    {
        switchingSidesText.text = message;
        switchingSidesText.enabled = true;
        yield return new WaitForSeconds(delay);
        switchingSidesText.enabled = false;
    }


    public void UpdateVisibleMoveOptions()
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                if ("w".Equals(currentTurn))
                {
                    if ("legal".Equals(whiteMoves[x, y]))
                    {
                        _tiles[x, y].gameObject.transform.Find("Highlight").gameObject.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                    _tiles[x, y].gameObject.transform.Find("Highlight").gameObject.GetComponent<Renderer>().enabled = false;
                    }
                }
                else if ("b".Equals(currentTurn))
                {
                    if ("legal".Equals(blackMoves[x, y]))
                    {
                        _tiles[x, y].gameObject.transform.Find("Highlight").gameObject.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                    _tiles[x, y].gameObject.transform.Find("Highlight").gameObject.GetComponent<Renderer>().enabled = false;
                    }
                }
            }
        }
    }

    private void UpdateMovesInts()
    {
        blackMovesInt = 0;
        whiteMovesInt = 0;
        for (int j = 0; j < 8; j++)
        {
            for (int k = 0; k < 8; k++)
            {
                if ("legal".Equals(blackMoves[j, k])) blackMovesInt++;
                if ("legal".Equals(whiteMoves[j, k])) whiteMovesInt++;
            }
        }
    }
}