﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float xMoveThreshold = 1.0f;
    public float yMoveThreshold = 1.0f;

//Y limit
    public float yMaxLimit = 45.0f;
    public float yMinLimit = -180.0f;
    float yRotCounter = 0.0f;

//X limit
    public float xMaxLimit = 90.0f;
    public float xMinLimit = -90.0f;
    float xRotCounter = 0.0f;


    void Update()
    {
        //Get X value and limit it
        xRotCounter += Input.GetAxis("Mouse X") * xMoveThreshold * Time.deltaTime;
        xRotCounter = Mathf.Clamp(xRotCounter, xMinLimit, xMaxLimit);

        //Get Y value and limit it
        yRotCounter += Input.GetAxis("Mouse Y") * yMoveThreshold * Time.deltaTime;
        yRotCounter = Mathf.Clamp(yRotCounter, yMinLimit, yMaxLimit);
        //xRotCounter = xRotCounter % 360;//Optional
        transform.localEulerAngles = new Vector3(-yRotCounter, 180+xRotCounter, 0);
    }
}