﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RatingScreenManager : MonoBehaviour
{
    [SerializeField]
    public Text highscoreText;
    [SerializeField]
    public Text highscoreNicksText;

    private string highScores;

    // Start is called before the first frame update
    void Start()
    {
        highScores = PlayerPrefs.GetString("highscores", "");
        
        if (highScores.Equals(""))
        {
            highscoreNicksText.text = "NO HIGHSCORES";
            highscoreText.text =  "AVAILABLE";
        }
        else
        {
            HighScore[] highScoresArray = JsonHelper.FromJson<HighScore>(highScores);
            List<HighScore> highScoreList = new List<HighScore>(highScoresArray);
            highScoreList.ForEach(highScore1 => AddHighScoreToText(highScore1));

        };
    }

    private void AddHighScoreToText(HighScore highScore1)
    {
        
        string nickSeparator ;
        if(highScore1.nick.Equals(""))
        {
            nickSeparator = "    " + "Anonymous";
        }
        else
        {
            nickSeparator = "    " + (highScore1.nick);
        }
        highscoreNicksText.text += "\n "+(highScore1.rank+1) + nickSeparator;
        highscoreText.text += "\n "+highScore1.score;
        
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }
    
    
    [Serializable]
    public class HighScore
    {
        public int rank;
        public int score;
        public string nick;
    }
    
    public static class JsonHelper
    {
        public static T[] FromJson<T>(string json)
        {
            Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
            return wrapper.Items;
        }

        [Serializable]
        private class Wrapper<T>
        {
            public T[] Items;
        }
    }
}
