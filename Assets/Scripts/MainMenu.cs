﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] public Button resumeButton;
    [SerializeField] public Button HumanSelectionButton;
    [SerializeField] public Button ComputerSelectionButton;
    [SerializeField] public Button StartButton;
    [SerializeField] public InputField nicknameField;
    private string _enemy = "human";
    private bool _minmax = true;
    private string nick = "";
    private bool isThereGameToResume = false;

    public void Start()
    {
        ResetTempPlayerPrefs();
        //check if there is load file
        isThereGameToResume = CheckIfThereIsFile();
        SetButtonsIn2ndMenu();
        if (!isThereGameToResume)
        {
            if (resumeButton)
            {
                var resumeButtonColors = resumeButton.colors;
                resumeButtonColors.normalColor = resumeButtonColors.disabledColor;
                var componentInChildren = resumeButton.GetComponentInChildren(typeof(TMP_Text)) as TMP_Text;
                if (componentInChildren != null) componentInChildren.text = "";
                resumeButton.interactable = false;
            }
        }
    }

    private bool CheckIfThereIsFile()
    {
        if (PlayerPrefs.GetString("SaveData") != null && !PlayerPrefs.GetString("SaveData").Equals(""))
        {    
            return true;
        }
        
        return false;
    }

    private static void ResetTempPlayerPrefs()
    {
        PlayerPrefs.DeleteKey("enemy");
        PlayerPrefs.DeleteKey("minmax");
        PlayerPrefs.DeleteKey("nick");
        PlayerPrefs.DeleteKey("winner");
        PlayerPrefs.DeleteKey("score");
        PlayerPrefs.DeleteKey("highscore");
        PlayerPrefs.DeleteKey("resuming");
    }

    private void SetButtonsIn2ndMenu()
    {
        _enemy = "human";
        if (HumanSelectionButton)
        {
            HumanSelectionButton.onClick.AddListener(() => { _enemy = "human"; });
            
        }

        if (ComputerSelectionButton)
        {
            ComputerSelectionButton.onClick.AddListener(() => { _enemy = "computer"; });
        }
    }

    public void Update()
    {
        if( Input.GetMouseButtonDown(0) )
        {
            Ray ray = Camera.main.ScreenPointToRay( Input.mousePosition );
            RaycastHit hit;
         
            if( Physics.Raycast( ray, out hit, 100 ) )
            {
                if (hit.transform.gameObject != ComputerSelectionButton.gameObject && hit.transform.gameObject != HumanSelectionButton.gameObject)
                {
                    StartButton.interactable = false;
                }
                else StartButton.interactable = true;
            }
        }
    }

    public void StartGame()
    {
        PlayerPrefs.SetString("enemy", _enemy);
        PlayerPrefs.SetString("minmax", _minmax.ToString());
        PlayerPrefs.SetString("nick", nick);
        SceneManager.LoadScene(2);
    } 
    public void GoToStartMenu()
    {
        SceneManager.LoadScene(1);
    }


    public void ResumeGame()
    {
        PlayerPrefs.SetString("resuming", "True");
        SceneManager.LoadScene(2);
    }

    public void Back()
    {
        SceneManager.LoadScene(0);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void GoToRatingScreen()
    {
        SceneManager.LoadScene("RatingScene");
    }  
    
    public void ToggleChanged()
    {
        _minmax = !_minmax;
        Debug.Log("Changed minmax to "+_minmax);
    } 
    
    public void NickChanged()
    {
     nick = nicknameField.GetComponentInChildren<Text>().text;
    }
}